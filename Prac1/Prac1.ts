class greeter{
    greeting: string;

    constructor(message: string)
    {
        this.greeting = message;
    }

    greet()
    {
        let str =  "Hello " + this.greeting;
        console.log(str)
    }
}

let objgreet = new greeter("Ankit");
objgreet.greet();           