var greeter = /** @class */ (function () {
    function greeter(message) {
        this.greeting = message;
    }
    greeter.prototype.greet = function () {
        var str = "Hello " + this.greeting;
        console.log(str);
    };
    return greeter;
}());
var objgreet = new greeter("Ankit");
objgreet.greet();
