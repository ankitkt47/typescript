var Account = /** @class */ (function () {
    function Account(accid, accname, balance) {
        this.acc_id = accid;
        this.acc_name = accname;
        this.currentBalance = balance;
        ++Account.count;
        console.log("Constructor Called");
    }
    Account.prototype.getAccountDetails = function () {
        console.log("Account ID " + this.acc_id);
        console.log("Account Name " + this.acc_name);
        console.log("Balance " + this.currentBalance);
    };
    Account.getObjectCount = function () {
        console.log("Total Objects " + Account.count);
    };
    Account.count = 0;
    return Account;
}());
var obj1 = new Account(12232, "Ankit Todankar", 10000);
var obj2 = new Account(12232, "Viraj Todankar", 20000);
var obj3 = new Account(12232, "Vihan Nagawekar", 30000);
obj1.getAccountDetails();
obj2.getAccountDetails();
obj3.getAccountDetails();
Account.getObjectCount();
