class Account
{

    private accid: number;
    private acc_name: string;
    private currentBalance: number;

    private static count:number = 0;

    constructor(id:number,name:string,balance:number){
        this.accid = id;
        this.acc_name = name;
        this.currentBalance = balance;

        ++Account.count;
        console.log("Constructor Called");
    }

    public getAccountDetails():void{
        console.log("Account ID "+ this.accid);
        console.log("Account Name "+ this.acc_name);
        console.log("Balance "+ this.currentBalance);
    }

    public static getObjectCount(){
        console.log("Total Objects "+ Account.count);
    }

}

let obj1 = new Account(12,"Ankit Todankar",10000);
let obj2 = new Account(13,"Viraj Todankar",20000);
let obj3 = new Account(14,"Vihan Nagawekar",30000);

obj1.getAccountDetails();
obj2.getAccountDetails();
obj3.getAccountDetails();
Account.getObjectCount();