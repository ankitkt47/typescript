class Scope
{

    identify():void
    {
        if( 1==1 )
        {
            var str1 = "declared using var";
            let str2 = "declared using let";
        }
        console.log(str1); // No error. var is globally available in function
        //console.log(str2); // cause error. let is available only in scope

    }

}